# Welcome

Welcome to Sebby's Wii homebrew repo!

## Help Wanted

Complete the form if you want to help me with this repo:
https://forms.gle/dhPsoMPVnov3Cyak6

## Please Note:

That [The First app on here](https://gitlab.com/sebbys-group/sebbys-wii-homebrew-app-repo/-/blob/main/Wii60.zip?ref_type=heads) is in english

You also need to hack your wii (Recommended guides below)

Don't forget that you'll need a SD Card (Or USB if you have a Wii Mini) (The USB Method will work with the Normal Wiis as well)

Follow [this guide](https://wii.hacks.guide/cios) to do cIOS crap

Follow [this other guide for Wii U](https://wii.hacks.guide/cios-vwii)

Follow [this different guide for Wii Mini cIOS crap](https://wii.hacks.guide/cios-mini)

Don't forget to install [Priiloader](https://gitlab.com/sebbys-group/sebbys-wii-homebrew-app-repo/-/tree/main/priiloader) and BootMii

Go to https://wii.hacks.guide/ for more great homebrew tutorials (And to correct me if there's any issues)

The NUS thing is for obtaining wii IOS/WiiWare/VC/etc.


## Wii Exploits

Go to https://wii.hacks.guide/get-started to get started with homebrewing
